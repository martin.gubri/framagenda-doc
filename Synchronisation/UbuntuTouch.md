# Synchronisation avec Ubuntu Touch
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Pour synchroniser Framagenda avec un UbuntuTouch, il faut :

* Aller dans Applications-Agenda
* Cliquer sur la première icône de la zone en haut à droite
* Cliquer sur Ajouter un agenda en ligne
* Cliquer sur ownCloud, et entrez les informations suivantes :
   - URL : `https://framagenda.org/`
   - Username : votre nom d'utilisateur
   - Password : votre mot de passe
* Cliquer sur Continue

La synchronisation se fait et votre Framagenda est visible.

