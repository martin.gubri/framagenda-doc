# Synchronisation avec GNOME
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

L'environnement de bureau GNOME (à partir de la version 3.8) permet de synchroniser ses agendas et ses contacts nativement et très facilement.

Allez dans les paramètres systèmes, puis cliquez sur **Comptes en ligne**.

![Paramètres Gnome](../img/gnome-1.png)

Cliquez ensuite sur le **+** pour ajouter un nouveau compte, puis choisissez **ownCloud**. Remplissez ensuite les champs de la manière suivante :

Adresse : https://framagenda.org

Identifiant : Votre identifiant sur Framagenda

Mot de passe : Votre mot de passe sur Framagenda

![Formulaire de connexion](../img/gnome-2.png)

Note : Si vous utilisez [l'authentification en deux étapes](../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa), vous devez créer [un mot de passe d'application](../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication).

Cliquez sur **Connexion** pour terminer la procédure.

Vous pouvez décocher la synchronisation pour les documents et les fichiers, Framagenda ne proposant pas ces services (voir [Framadrive](https://framadrive.org) à la place)

![Synchronisation Gnome](../img/gnome-3.png)

Vos agendas sont maintenant disponibles dans les applications Gnome Agenda, Gnome Contacts, et le *groupware* Evolution.
