# Synchronisation avec Apple Calendrier (iCal)
[<span class="glyphicon glyphicon-arrow-left"></span> Retour à l'accueil](../README.md)

Allez dans les préférences système, puis cliquez sur **Comptes Internet**.

Cliquez sur **Ajouter un autre compte** puis choisissez **Ajouter un compte CalDAV**.

Dans la fenêtre **Ajouter un compte CalDAV** puis renseignez votre identifiant et votre mot de passe, et entrez comme adresse du serveur celle affichée pour iOS/OSX dans les paramètres de framagenda (tout en bas à gauche), et cliquez sur **Créer**.

![Adresse Serveur OSX](../img/agenda-5.png)

Vos agendas devraient maintenant être ajoutés dans l'application Calendrier de MacOS X.
